const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
	// transactionNo: {
	// 	type: Number,
	// 	required: [true, "Add position number"]
	// },
	item: [
		{
			productId: {
				type: String,
				required: [true, "Email is required"]
			},
			quantity: {
				type: Number,
				required: [true, "Order quantity can't be 0"]
			},
			price: {
				type: Number,
				required: [true, "Price of the product is required"]
			}
		}
	],
	totalAmount: {
		type: Number,
		required: [true, "Added amount of all items"]
	},
	purchasedOn: {
		type: Date,
		default: new Date()
	}
})

module.exports = mongoose.model("Order", orderSchema);