const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	email: {
		type: String,
		required: [true, "Email is required"]
	},
	password: {
		type: String,
		required: [true, "Password is required"]
	},
	mobileNo: {
		type: Number,
		required: [true, "Mobile number is required"]
	},
	firstName: {
		type: String,
		required: [true, "First name is required"]
	},
	lastName: {
		type: String,
		required: [true, "Last name is required"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	orders: [
		{
			orderId: {
				type: String,
				required: [true, "Order no. is required"]
			},
			totalAmount: {
				type: Number,
				required: [true, "The total price of purchase is required"]
			},
			dateOrdered: {
				type: Date,
				default: new Date()
			},
			status: {
				type: String,
				default: "Pending"
			}
		}
	],
	cart: [
		{
			productId: {
				type: String,
				required: [true, "Product no is required"]
			},
			quantity: {
				type: Number,
				required: [true, "Order quantity can't be 0"]
			},
			price: {
				type: Number,
				required: [true, "Product price is required"]
			}
		}
	]
})

module.exports = mongoose.model("User", userSchema);