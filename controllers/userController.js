const User = require("../models/User.js");
const Product = require("../models/Product.js");
const Order = require("../models/Order.js");
const auth = require("../auth.js");

const bcrypt = require("bcrypt");

module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password,10),
		mobileNo: reqBody.mobileNo,
		firstName: reqBody.firstName,
		lastName: reqBody.lastName
	})

	return newUser.save().then((user,error) => {
		if (error) {
			return false
		}
		else {
			return true
		}
	})
}

module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if (result===null){
			return 'This email is not a registered user.';
		}
		else {
			//return true
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			if (isPasswordCorrect) {
				return {access: auth.createAccessToken(result)}
			}
			else {
				return 'Incorrect password.'
			}
		}
	})
}

module.exports.getProfile = (data) => {
	console.log(data);
	return User.findById(data.userId).then(result => {

		// Changes the value of the user's password to an empty string when returned to the frontend
		// Not doing so will expose the user's password which will also not be needed in other parts of our application
		// Unlike in the "register" method, we do not need to call the mongoose "save" method on the model because we will not be changing the password of the user in the database but only the information that we will be sending back to the frontend application
		result.password = "";

		// Returns the user information with the password as an empty string
		return result;
	});
};

module.exports.getCart = (data) => {
	console.log(data);
	return User.findById(data.userId).then(result => {

		// Changes the value of the user's password to an empty string when returned to the frontend
		// Not doing so will expose the user's password which will also not be needed in other parts of our application
		// Unlike in the "register" method, we do not need to call the mongoose "save" method on the model because we will not be changing the password of the user in the database but only the information that we will be sending back to the frontend application
		result.password = "";
		result.orders = [];
		result.mobileNo = "";
		result.isAdmin = "";

		// Returns the user information with the password as an empty string
		return result;
	});
};

module.exports.getOrder = (data) => {
	console.log(data);
	return User.findById(data.userId).then(result => {

		// Changes the value of the user's password to an empty string when returned to the frontend
		// Not doing so will expose the user's password which will also not be needed in other parts of our application
		// Unlike in the "register" method, we do not need to call the mongoose "save" method on the model because we will not be changing the password of the user in the database but only the information that we will be sending back to the frontend application
		result.password = "";
		result.cart = [];
		result.mobileNo = "";
		result.isAdmin = "";

		// Returns the user information with the password as an empty string
		return result;
	});
};
module.exports.getLastOrder = (data) => {
	//console.log(data);
	return Order.find().sort({purchasedOn:-1}).limit(1)
};

module.exports.getAllOrders = (data) => {
	if (data.isAdmin) {
		return Order.find().then(result => {
			return result;
		});
	}
	else {
		return false
	}
};


module.exports.setUserAdmin = (isAdmin, data) => {
	if (isAdmin) {
		return User.updateOne({_id: data},{isAdmin: true}).then((user,err) =>{
			if (err) {
				return false
			}
			else {
				return true
			}
		})
	}
	else {
		return false
	}
}

module.exports.cartToOrder = async (data, reqBody) => {
	let total = 0;
	for (let i = 0; i < reqBody.cart.length; i++) {
		total += reqBody.cart[i].price*reqBody.cart[i].quantity;
	}
	let newOrder = new Order ({
			item: reqBody.cart,
			totalAmount: total
		})
	let isNewOrder = await newOrder.save().then((order,error) => {
		if (error) {
			return false
		}
		else {
			return true
		}
	})
	let isCartEmpty = await User.findById(data.id).then(user => {
		user.cart = [];
		return user.save().then((user,error) => {
			if (error) {
				return false
			}
			else {
				return true
			}
		})
	})
	let isOrderRecorded = await User.findById(data.id).then((user) => {
		return userOrderUpdate = Order.findOne().sort({purchasedOn:-1}).limit(1).then((order)=>{
		//console.log(order)
		//console.log(order)
			let pushed = user.orders.push({
				orderId: order.id,
				totalAmount: order.totalAmount,
				dateOrdered: order.purchasedOn
			})
			return user.save().then((user,err) => {
				if (err) {
					return false
				}
				else {
					//console.log(user)
					return true
				}
			})
		})
	})
	if (isNewOrder&&isCartEmpty&&isOrderRecorded) {
		return true
	}
	else {
		return false
	}

}
// module.exports.cartToOrder = async (data) => {
// 	console.log(data);
// 	// let product = Product.findOne({id: data.productId});
// 	let popped = [];
// 	/*let isUserUpdated = await User.findById(data.id).then(user => {
// 		for (let i = 0; i < user.cart.length; i++) {
// 			popped += user.cart.pop()
// 			return user.save().then((user,error) => {
// 				if (error) {
// 					return false
// 				}
// 				else {
// 					return true
// 				}
// 			})
// 		}
// 	})*/
// 	let isUserUpdated = await User.findById(data.id).then(user => {
// 		// console.log(user);
// 		// console.log(data.productId);
// 			// console.log(product);
// 			// console.log(product.price);
// 			// console.log(user);
// 		for (let i = 0; i < user.cart.length; i++) {
// 			popped += user.cart.pop()
// 			user.save().then((user,error) => {
// 				if (error) {
// 					return false
// 				}
// 				else {
// 					return true
// 				}
// 			})
// 		}
// 		let total;
// 		for (let i = 0; i < popped.length; i++) {
// 			total += popped[i].price;
// 		}
// 			newOrder = new Order ({
// 				items: popped,
// 				totalAmount: total
				
// 			});
// 			return newOrder.save().then((course,error) => {
// 				if (error) {
// 					return false
// 				}
// 				else {
// 					//message = `You have successfully created ${newCourse.name}!`
// 					return true
// 				}
// 			})
// 	})
// 	/*let isCartUpdated = await User.findById(data.userId).then(user => {
// 		user.enrollees.push({userId: data.userId});
// 		return course.save().then((course,error) => {
// 			if (error) {
// 				return false
// 			}
// 			else {
// 				return true
// 			}
// 		})
// 	})*/
// 	}
// 	if (isUserUpdated) {
// 		return true
// 	}
// 	else {
// 		return false
// 	}
// /*module.exports.cartToOrder = async (data) => {
// 	let isOrderUpdated = await User.findOne({id: data.userId}).then(user => {
// 		let total, items = user.cart;
// 		for (let i = 0; i < items.length; i++) {
// 			let product = Product.findById(items.productId)
// 			total += items[i].quantity * product.price
// 		}
// 		let transactionHistory = user.orders.length + 1
// 		let newOrder = new Order({
// 			transactionNo: transactionHistory,
// 			item: items,
// 			totalAmount: total
// 		})
// 		return newOrder.save().then((user,error) => {
// 			if (error) {
// 				return false
// 			}
// 			else {
// 				return true
// 			}
// 		})
// 	})
// 	let isUserUpdated = await Order.find(({transactionNo:-1}).limit(1)).then(order => {
// 		user = User.find({id: userData.id});
// 		updatedUser = 
// 			{	
// 				cart: [{}],
// 				orders: [{
// 					orderId: order.id,
// 					totalAmount: order.totalAmount
// 				}]
// 			}
// 		return updatedUser.update().then((user,error) => {
// 			if (error) {
// 				return false
// 			}
// 			else {
// 				return true
// 			}
// 		})
// 	})
// 	let isProductUpdated = await Order.find(({transactionNo:-1}).limit(1)).then(order => {
// 		order.items.forEach(function(item){
// 			let updatedProduct = Product.findOne({name: item.name}).then(product => {product.stock = product.stock - item.quantity})
// 			updatedProduct.update()
// 		})
// 		return product.save().then((course,error) => {
// 			if (error) {
// 				return false
// 			}
// 			else {
// 				return true
// 			}
// 		})
// 	})
// 	if (isUserUpdated && isCourseUpdated) {
// 		return true
// 	}
// 	else {
// 		return false
// 	}
// }*/