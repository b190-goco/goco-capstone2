const Product = require("../models/Product.js");
const User = require("../models/User.js");
const Order = require("../models/Order.js");
const auth = require("../auth.js");

const bcrypt = require("bcrypt");
/*
	MINI ACTIVITY
	create a new course object using the mongoose model 
	save the new course object to the database

*/


module.exports.addProduct = (isAdmin, reqBody) => {
	// console.log(isAdmin);
	// console.log(reqBody);
	//let message;
	if (isAdmin === true) {
		if (reqBody.name !== null || reqBody.description !== null || reqBody.price !== null) {
			let newProduct = new Product ({
						name: reqBody.name,
						description: reqBody.description,
						price: reqBody.price,
						stock: reqBody.stock
					});
		
				return newProduct.save().then((course,error) => {
					if (error) {
						return false
					}
					else {
						//message = `You have successfully created ${newCourse.name}!`
						return true
					}
				})
		}
	}
	else {
		//message = "You are not allowed to access this function"
		return false
	}
}

module.exports.getAllProducts = () => {
	return Product.find({ }).then(result => {
		if (result.length > 0) {
			return result
		}
		else {
			return false
		}
	})
}

module.exports.getAllActive = () => {
	return Product.find({isActive: true}).then(result => {
		if (result.length > 0) {
			return result
		}
		else {
			return false
		}
	})
}
module.exports.getProduct = (productId) => {
	//console.log(courseId)
	return Product.find({_id: productId}).then(result => {
		if (result.length > 0) {
			return result
		}
		else {
			return false
		}
	})
}

module.exports.updateProduct = (reqParams, reqBody) => {
	let updatedProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		stock: reqBody.stock,
	}
	return Product.findByIdAndUpdate(reqParams,updatedProduct).then((product,err) => {
		if (err) {
			return false
		}
		else {
			console.log(product)
			return true
		}
	})
}
// isolated updating 'isActive' from the updating course
// incorporated admin credentials for accessing this function
/*module.exports.archiveCourse = (isAdmin, reqParams, reqBody) => {
	// console.log(isAdmin);
	// console.log(reqBody);
	// let message;
	if (isAdmin === true) {
		if (reqBody.name !== null || reqBody.description !== null || reqBody.price !== null) {
			let updatedProduct = {
				isActive: reqBody.isActive
			}
			return Product.updateOne({id: reqParams},updatedCourse).then((course,err) => {
				if (err) {
					return false
				}
				else {
					return true
				}
			})
		}
	}
	else {
		// message = "You are not allowed to access this function"
		return false
	}
}*/

module.exports.archiveProduct = (isAdmin, reqParams) => {
	if (isAdmin) {
		return Product.updateOne({_id: reqParams},{isActive: false}).then((product,err) =>{
			if (err) {
				return false
			}
			else {
				return true
			}
		})
	}
	else {
		return false
	}
}

/*module.exports.addToCart =  (data, reqParams, reqBody) => {
	return isUserUpdated = User.findOne({id: data.userId}).then(user => {
		let productInfo = Product.findOne({id: reqParams.id});
		console.log(productInfo);
		let productToCart = {
			productId: productInfo.id,
			name: productInfo.name,
			quantity: reqBody.quantity,
			price: productInfo.price
		}
		user.cart.push({productToCart});
		return User.updateOne().then((user,error) => {
			if (error) {
				return false
			}
			else {
				return user
			}
		})
	})
	// if (isUserUpdated) {
	// 	return true
	// }
	// else {
	// 	return false
	// }
}*/

/*module.exports.addToCart = (data, reqParams, reqBody) => {
	const productInfo = Product.find({_id: reqParams});
	console.log(reqBody);
	console.log(reqParams);
	console.log(data);
	let productToCart = {
		productId: productInfo.id,
		name: productInfo.name,
		quantity: reqBody.quantity,
		price: productInfo.price
	}
	console.log(productToCart);
	return User.updateOne({id: data},productToCart).then((cart,err) => {
		console.log(cart);
		if (err) {
			return false
		}
		else {
			return true
		}
	})
}*/

/*module.exports.updateCourse = (reqParams, reqBody) => {
	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		isActive: reqBody.isActive
	}
	return Course.updateOne({id: reqParams},updatedCourse).then((course,err) => {
		if (err) {
			return false
		}
		else {
			return true
		}
	})
}*/

module.exports.addToCart = async (data,reqBody) => {
	//console.log(product);
	// let product = Product.findOne({id: data.productId});
	let isUserUpdated = await User.findById(data.userId).then(user => {
		// console.log(user);
		// console.log(data.productId);
		return isPriceUpdated = Product.findById(data.productId).then(product => {
			// console.log(product);
			// console.log(product.price);
			// console.log(user);
			if (product.isActive) {
				let pushed = user.cart.push({
					productId: data.productId,
					quantity: reqBody.quantity,
					price: product.price
				})
				// console.log(pushed);
				return user.save().then((user,error) => {
					if (error) {
						return false
					}
					else {
						return true
					}
				})
			}
			else {
				return false
			}
		})
	})
	/*let isCartUpdated = await User.findById(data.userId).then(user => {
		user.enrollees.push({userId: data.userId});
		return course.save().then((course,error) => {
			if (error) {
				return false
			}
			else {
				return true
			}
		})
	})*/
	if (isUserUpdated) {
		return true
	}
	else {
		return false
	}
}