const User = require("../models/User.js");
const Product = require("../models/Product.js");
const Order = require("../models/Order.js");
const auth = require("../auth.js");

const bcrypt = require("bcrypt");
