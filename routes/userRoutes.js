const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController.js");
const auth = require("../auth.js")

// router.post('/checkEmail', (req,res) => {
// 	userController.checkEmailExists(req.body).then(resultFromController => res.send(
// 		resultFromController));
// });

router.post('/register', (req,res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(
		resultFromController));
});

router.post("/login", (req,res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(
		resultFromController));
});

router.get("/details", auth.verify, (req,res) => {
	const userData = auth.decode(req.headers.authorization);
	// console.log(userData);
	userController.getProfile({userId: userData.id}).then(resultFromController => res.send(
		resultFromController));
});

router.get("/viewcart", auth.verify, (req,res) => {
	const userData = auth.decode(req.headers.authorization);
	//console.log(userData);
	userController.getCart({userId: userData.id}).then(resultFromController => res.send(
		resultFromController));
});

router.get("/orders", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization); // recognizes login token
	//console.log(userData);
	userController.getOrder({userId: userData.id}).then(resultFromController => res.send(
		resultFromController));
});

router.get("/allOrders", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization); // recognizes login token
	//console.log(userData);
	userController.getAllOrders(userData).then(resultFromController => res.send(
		resultFromController));
});
router.get("/lastOrder", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization); // recognizes login token
	//console.log(userData);
	userController.getLastOrder(userData).then(resultFromController => res.send(
		resultFromController));
});

router.post("/checkout", auth.verify, (req,res) => {
	const userData = auth.decode(req.headers.authorization);
	userController.cartToOrder(userData, req.body).then(resultFromController => res.send(
		resultFromController));
});

router.put("/:id/setAsAdmin", auth.verify, (req,res) => {
	const userData = auth.decode(req.headers.authorization);
	userController.setUserAdmin(userData, req.params.id).then(resultFromController => res.send(
		resultFromController));
});

module.exports = router;
