const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController.js");
const auth = require("../auth.js");

router.post("/add", auth.verify, (req,res) => {
	const userData = auth.decode(req.headers.authorization);
	//console.log(userData);
	productController.addProduct(userData.isAdmin, req.body).then(resultFromController =>
	res.send(resultFromController));
})

router.get("/all", (req,res) => {
	productController.getAllProducts().then(resultFromController =>
		res.send(resultFromController))
})


router.get("/active", (req,res) => {
	productController.getAllActive().then(resultFromController =>
		res.send(resultFromController));
})
/*
const userData = auth.decode(req.headers.authorization);
	console.log(userData);
	userController.getProfile({userId: userData.id}).then(resultFromController => res.send(
		resultFromController));
*/

router.get("/:id", (req,res) => {
	productController.getProduct(req.params.id).then(resultFromController => 
		res.send(resultFromController));
})

/*router.post("/:id/addtocart", auth.verify, (req,res) => {
	const userData = auth.decode(req.headers.authorization);
	productController.addToCart(userData.id, req.params.id, req.body).then(resultFromController => res.send(
		resultFromController));
});
*/
router.post("/:id/addtocart", auth.verify, (req,res) => {
	const userData = auth.decode(req.headers.authorization);
	let data = {
		userId: userData.id,
		productId: req.params.id
	};
	console.log(data);
	productController.addToCart(data, req.body).then(resultFromController => res.send(
		resultFromController));
});

router.put("/:id/update", auth.verify, (req,res) => {
	productController.updateProduct(req.params.id,req.body).then(resultFromController =>
		res.send(resultFromController));
})

// isolated the changing of 'isActive' from the update course
// incorporated admin credentials
router.put("/:id/archive", auth.verify, (req,res) => {
	const userData = auth.decode(req.headers.authorization);
	//console.log(userData);
	productController.archiveProduct(userData.isAdmin, req.params.id).then(resultFromController =>
		res.send(resultFromController));
})

module.exports = router;