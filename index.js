/*
	on start up:

	git init
	npm init -y
	npm install express mongoose cors bcrypt jsonwebtoken


*/

const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

//routes
const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");

const app = express();

// mongoDB admin
// username: jygsgoco
// password: peanutz11151998
mongoose.connect("mongodb+srv://jygsgoco:peanutz11151998@wdc028-course-booking.ssabc5g.mongodb.net/b190-Capstone-2?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log("We're connected to the database"));

app.use(cors());

app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.use("/users", userRoutes);
app.use("/products", productRoutes);

app.listen(process.env.PORT || 4000, () => console.log(`API now online at port: ${process.env.PORT || 4000}`));

/*
	Heroku deployment
		Procfile - needed file in heroku to determine the command that will be used when starting the server (web: node app)

		steps in deploying:
		REMINDER: use the following commands **inside the root folder of the project (where the index.js is)

		1. heroku login - you'll be redirected to the browser for authentication
		2. heroku create - must be done at project's root directory
									 - an app name will be auto-generated for you
									 - this will also automatically add a remote repository named heroku to your local repo, you can verify this with git remote -v
		3. use git commands to add, commit and push to heroku with "git push heroku master"

		NOTE: everytime there are updates with the project, we only have to use git add/commit/push to update the repo (both in gitlab and heroku)
*/
